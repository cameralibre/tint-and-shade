# Tint & Shade

This is my first time using [Vue.js](https://vuejs.org)!

When someone selects a color, the resulting value is converted from a hex code to HSL (hue, saturation & lightness) values. 
Then some basic computation is applied to those values in order to automatically create a set of useful related colors which can be used in a composition, together with the base color.
![Screenshot showing a grid of the chosen base color and 14 related colors](./screenshot.png)

I've now added an SVG upload option:

![](./tint-and-shade.png)

Try it out at [tint-and-shade.hashbase.io](https://tint-and-shade.hashbase.io)

or (using [Beaker](https://beakerbrowser.com/)):

dat://9f0919e590bd216af1911966d8a09623ec7fa879a287dab5a908c7520691c61d/

## Next steps: 

- [x] Make different colors in the palette directly selectable
- [x] Make all shapes in the uploaded SVG clickable
- [x] apply the selected color to the clicked shapes.
- [ ] if the shape has a stroke but no fill, add the selected color to the stroke