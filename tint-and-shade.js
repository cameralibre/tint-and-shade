let swatchComponent = {
  template: '#swatch-template',
  props: {
    color: {
      type: String,
      required: true
    },
    selectedSwatch: {
      type: String
    }
  },
  computed: {
    isSelected () {
      return this.color === this.selectedSwatch
    }
  },
  methods: {
    select () {
      this.$emit('select', this.color)
    }
  }
}

new Vue({
  el: '#app',
  components: {
    swatch: swatchComponent
  },
  data () {
    return {
      hexcode: '#1aa1e6',
      currentColor: '#1aa1e6',
      selectedSwatch: '#1aa1e6',
      upload: '',
      shapes: []
    }
  },
  computed: {
    swatchesArray: function () {
      hslValues = this.hexToHsl(this.hexcode)
      let h = hslValues[0]
      let s = hslValues[1]
      let l = hslValues[2]
      let complement = (hslValues[0] + 180) % 360
      let tint15 = Math.min((hslValues[2] + 15), 100)
      let tint30 = Math.min((hslValues[2] + 30), 100)
      let shade15 = Math.max((hslValues[2] - 15), 0)
      let shade30 = Math.max((hslValues[2] - 30), 0)
      let inverseSat = 100 - hslValues[1]
      let baseColor = this.Hsl(h,s,l)
      let baseTint15 = this.Hsl(h, s, tint15)
      let baseTint30 = this.Hsl(h, s, tint30)
      let baseShade15 = this.Hsl(h, s, shade15)
      let baseShade30 = this.Hsl(h, s, shade30)
      let complementColor = this.Hsl(complement, s, l)
      let complementTint15 = this.Hsl(complement, s, tint15)
      let complementTint30 = this.Hsl(complement, s, tint30)
      let complementShade15 = this.Hsl(complement, s, shade15)
      let complementShade30 = this.Hsl(complement, s, shade30)
      let inverseSatColor = this.Hsl(complement, inverseSat, l)
      let inverseSatTint15 = this.Hsl(complement, inverseSat, tint15)
      let inverseSatTint30 = this.Hsl(complement, inverseSat, tint30)
      let inverseSatShade15 = this.Hsl(complement, inverseSat, shade15)
      let inverseSatShade30 = this.Hsl(complement, inverseSat, shade30)

      return [baseTint30, baseTint15, baseColor, baseShade15, baseShade30, complementTint30, complementTint15, complementColor, complementShade15, complementShade30, inverseSatTint30, inverseSatTint15, inverseSatColor, inverseSatShade15, inverseSatShade30]
    }
  },
  methods: {
    displaySVG () {
      let file = document.getElementById('upload').files[0]
      fileName = document.getElementById('upload').files[0].name
      let text = new Response(file).text()

      let data = this

      text.then(function(result){
        svgData = result
        let display = document.getElementById('image')
        display.innerHTML = result
        let displayedSVG = document.getElementsByTagName('svg')[0]
        SVG.adopt(displayedSVG)
        const paths = SVG.select('path').members
        const circles = SVG.select('circle').members
        const rects = SVG.select('rect').members
        const polygons = SVG.select('polygon').members
        const ellipses = SVG.select('ellipse').members
        const uses = SVG.select('use').members
        const texts = SVG.select('text').members
        const shapes = paths.concat(circles, rects, polygons, ellipses, uses, texts)

        data.shapes = shapes
        data.shapes.forEach(shape => {
          shape.click(function () {
            this.attr('fill', data.selectedSwatch)
          })
        })
      })
    },

    selectSwatch (swatch) {
      this.selectedSwatch = swatch
    },

    hexToHsl () {
      let r = parseInt(this.hexcode.slice(1, 3), 16),
          g = parseInt(this.hexcode.slice(3, 5), 16),
          b = parseInt(this.hexcode.slice(5, 7), 16);
      return this.rgbToHsl(r, g, b)
    },

    rgbToHsl(r, g, b) {
      r = +r/255, g = +g/255, b = +b/255;
      let max = Math.max(r, g, b), min = Math.min(r, g, b);
      let h, s, l = (max + min) / 2;
      if(max == min){
          h = s = 0; // achromatic
      }else{
          let d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
          switch(max){
              case r: h = (g - b) / d + (g < b ? 6 : 0); break;
              case g: h = (b - r) / d + 2; break;
              case b: h = (r - g) / d + 4; break;
          }
          h /= 6;
      }
      h = Math.floor(h * 360)
      s = Math.floor(s * 100)
      l = Math.floor(l * 100)
      return [h, s, l]
    },

    Hsl (h, s, l) {
      return "hsl(" + h + ", " + s + "%, " + l + "%)"
    }
  }
})
